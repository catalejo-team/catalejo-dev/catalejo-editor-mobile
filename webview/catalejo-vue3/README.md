# catalejo-vue3

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Packages

`npm i --save @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/vue-fontawesome@prerelease @fortawesome/free-brands-svg-icons @fortawesome/free-regular-svg-icons`

`npm i blockly --save-dev`

`npm i vue-float-menu --save-dev `

`npm i vue-property-decorator --save-dev`
