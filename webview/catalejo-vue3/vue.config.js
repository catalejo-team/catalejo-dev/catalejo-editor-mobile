const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
// const { VueLoaderPlugin } = require('vue-loader')

// vue.config.js
module.exports = {
  devServer: {
    port: 3000
  },

  publicPath: process.env.NODE_ENV === 'production' ? '' : '/',

  configureWebpack: {
    plugins: [
      // Copy over media resources from the Blockly package
      new CopyPlugin([
        {
          from: path.resolve(__dirname, './node_modules/blockly/media'),
          to: path.resolve(__dirname, 'dist/media')
        },
        {
          from: path.resolve(__dirname, './src/blockly-config/luabot/imag'),
          to: path.resolve(__dirname, 'dist/img/imag')
        }
      ])
      // new VueLoaderPlugin()
    ]
  },
  chainWebpack: config => {
    // https://github.com/vuejs/vue-next/issues/1414
    config.module.rule('vue')
      .use('vue-loader')
      // .loader('vue-loader')
      .tap(options => {
        options.compilerOptions = {
          ...(options.compilerOptions || {}),
          // isCustomElement: tag => 'xml'.test(tag)
          isCustomElement: tag =>
            tag === 'xml' || tag === 'field' || tag === 'block' ||
            tag === 'value' || tag === 'statement' || tag === 'mutation' ||
            tag === 'arg' || tag === 'variables' || tag === 'variable'
        }
        return options
      })
  },

  lintOnSave: false
}
