Blockly.Xml.domToWorkspace(document.getElementById('startBlocks'),workspace);


// Show code python
function showCode() {
	Blockly.Python.INFINITE_LOOP_TRAP = null;
	var code = 'import pyb\nfrom pyb import Pin\n';
	code += Blockly.Python.workspaceToCode(workspace);
	alert(code);
}

//Run code python
function runCode(){
	Blockly.Python.INFINITE_LOOP_TRAP = null;
  	var code = 'import pyb\npyb.LED(1).on()\n\x05\n\nfrom pyb import Pin\npyb.LED(1).off()\n';
  	code += Blockly.Python.workspaceToCode(workspace);
	code += '\x04\n';
	enviar(code);
}

//Save code python
function saveCode(){
	Blockly.Python.INFINITE_LOOP_TRAP = null;
  	var code = 'import pyb\npyb.LED(1).on()\n\x05\n\npyb.delay(1000)\nfrom pyb import Pin\npyb.LED(1).off()\n';
  	code += Blockly.Python.workspaceToCode(workspace);
	code += '\x04\n';
	var codetmp = code.split('\n');
	code = "";
	for (var i = 0, l = codetmp.length; i < l; i++) {
		code += "save__data"+codetmp[i]+'\n';
	}
	//console.log("save__begin\n"+code+"save__end\n");
	enviar("save__begin\n"+code+"save__end\n");
}

//Remove code python
function removeCode(){
	enviar("save__rm\n");
}

function importBlocks(){
	var x = document.getElementById("openFile");
	var reader = new FileReader();

	reader.onload = function(e) {
	var xml_text = reader.result;
	var xml = Blockly.Xml.textToDom(xml_text);
	Blockly.Xml.domToWorkspace(xml, workspace);
	//console.log(xml);
	}
	reader.readAsText(x.files[0]);
}

// Save Blocks xml
var nameFileproyect = "";
function exportBlocks(){
	if (nameFileproyect == null){
		nameFileproyect = "uname";
	}
	nameFileproyect = prompt("Nombre del Proyecto", nameFileproyect);
	if(nameFileproyect != null && nameFileproyect != ""){
		var xml = Blockly.Xml.workspaceToDom(workspace);	
		var xml_text = Blockly.Xml.domToText(xml);
		var blob = new Blob([xml_text], {type: "text/plain;charset=utf-8"});
		saveAs(blob, nameFileproyect+".xml");
	}
}

//*****	BEGIN WEBSOCKET ******//
// Connect2medialab
var mysocket;
var card_ip = "";

function connect2medialab(){
	if (card_ip == null){
			card_ip = "192.168.4.1";
	}
    var cardIP = prompt("Ingrese la dirección IP de la tarjeta a programar", card_ip);
	if (cardIP != null && cardIP != ""){
		card_ip = cardIP;
		mysocket = new WebSocket(`ws://${cardIP}:3335`);
		waitForSocketConnection(mysocket, webSocketHandler("\r\n"));
	} 
    else if (cardIP == ""){
		cardIP = card_ip;
		alert("Dirección IP inválida.");
	}
	else if (cardIP == null){
		cardIP = card_ip;
		alert("Comunicación cancelada.");
	}
	console.log(`ip:${cardIP}`);
}

// DisconnectMedialab
function disconnectWebsocket(){
    mysocket.close();
}

// Connection WebSocket
function waitForSocketConnection(socket, callback) {
  setTimeout(
    function () {
      if (socket.readyState == 1) {
        console.log("Connection is made")
		alert("Conexión establecida.");
        if(callback != null){
          callback();
        }
        return;
      } else {
        console.log("Connecting timeOut.")
		alert("Se excedió el tiempo de espera.");
      }
  }, 4000); // wait 4000 milisecond for the connection...
}

function webSocketHandler(onMessageMsg) {
  mysocket.onopen = function() {
    console.log('Connection opened.');  
	alert("Abriendo conexión...");
    connectionState = 1;
    //mysocket.send(onMessageMsg);
  }
  
  mysocket.onclose = function(e) {
	console.log("WebSocket Closed.");
	alert("Conexión cerrada.");
    connectionState = 0;
  }

  mysocket.onmessage = function(e) {
	document.getElementById("areaReceive").value =
	e.data + document.getElementById("areaReceive").value;
	lineCounter = lineCounter + 1;
	if(lineCounter < lineas.length){
		mysocket.send(lineas[lineCounter]+"\r");
		document.getElementById("areaSend").value =
		document.getElementById("areaSend").value + lineas[lineCounter] + "\r\n";
		if (lineCounter == (lineas.length-1)){
			alert("Programa enviado a Media_Lab y ejecutandose");
		}
	} else if(e.data.search("MicroPython")>-1){
		lineCounter = 0;
		lineas = [];
		document.getElementById("areaReceive").value = e.data;
		alert("Reset en tarjeta Media_Lab")
	}
	  else if(lineCounter == lineas.length){
		lineCounter = 0;
		lineas = [];
		alert("Ejecución en Media_Lab terminada");
	}
  }
}

// Start Send Code
var lineCounter = 0;
var lineas = [];

function enviar(code){	
  lineCounter = 0;
  lineas = [];
  document.getElementById("areaSend").value = "";
  document.getElementById("areaReceive").value = "";
  lineas = code.split('\n');
  //console.log(lineas);
  mysocket.send(lineas[lineCounter]+"\r");
  document.getElementById("areaSend").value =
  document.getElementById("areaSend").value + lineas[lineCounter] + "\r\n";	
}
// End send code
//*****	END WEBSOCKET ******//
