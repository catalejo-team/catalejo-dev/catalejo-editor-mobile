# Encendido de LED a través de WIFI

![led_http](../img/led_http/baner-blink-http.jpeg)

## Ejemplo de montaje

Según los materiales que tengas usa **cualquiera** de los siguientes montajes:

### Montaje 1

![led blink](../img/intro/blink/nodemcu_led_bb.png)

### Montaje 2

![led_http](../img/led_http/led_on_off_http_bb.png)

## Creación de Algoritmo

Cortesía de:
<p style="color:blue;">Isabella <br>discord: IsabellaRopain#8294</p>

![led_http](../img/led_http/led_http_luabot.png)

Después de crear el programa debes:

1. Conéctate a la tarjeta por WiFi
2. Envia el programa
2. Oprimir el botón **RST** para reiniciar la tarjeta
3. Revisa que sigues conectada a la tarjeta desde tu celular
4. Entra al navegador Web del celular e introduce la siguiente dirección ip **192.168.4.1**
te deberá aparecer la página web que tú creaste

![navegador y dirección ip](../img/led_http/pagina-web.jpeg){: style="width:70%; margin-left: auto; margin-right: auto; display: block"}

* Finalmente interactúa con los botones **ON** y **OFF** para encender o apagar el LED.
