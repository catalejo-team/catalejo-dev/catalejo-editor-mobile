# Servomotor

Dispositivo que tiene un brazo que puede ser posicionado en un ángulo.

* El ángulo puede ser entre [0 - 180] grados
* La fuerza de torsión es de un kilogramo de fuerza.

## Ejemplos

### Cambio de posición manual

Aquí te invitamos a jugar con el ángulo en el que desees que se ponga el brazo, en el ejemplo, el ángulo inicial
es 0 grados, al pasar un segundo el brazo se ubicará en el ángulo de 90 grados donde podrá generar hasta una
fuerza de torsión de un kilogramo, pasado un segundo se apagará. Juega cambiando el angulo y el tiempo, inclusive
agrega más pasos para que cambie entre más ángulos antes de apagarse.

#### Conexiones NodeMCU V3

![conexiones](../img/actuadores/servomotor/nodemcu_servomotor_bb.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

#### Algoritmo

![algoritmo](../img/actuadores/servomotor/servomotor.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}



