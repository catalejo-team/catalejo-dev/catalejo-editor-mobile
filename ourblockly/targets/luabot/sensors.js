/*
 *  read any dht sensor
 */
Blockly.Blocks['dht_read'] = {
  init: function() {
    this.appendValueInput("pin")
        .setCheck("Number")
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/dht.gif", 20, 15, "*"))
        .appendField("leer DHT en Pin");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['dht_read'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'pin', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'dhtStatus, dhtTemp, dhtHum, dhtTemp_dec, dhtHum_dec = dht.read11('+value_pin+')\n';
  return code;
};

/*
 * dht view variables
 */
Blockly.Blocks['dht_view'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/dht.gif", 20, 15, "*"))
        .appendField("DHT.")
        .appendField(new Blockly.FieldDropdown([["Temperatura", "dhtTemp"], ["Humedad", "dhtHum"], ["Estado sensor", "dhtStatus"], ["Temperatura decimal", "dhtTemp_dec"], ["Humedad decimal", "dhtHum_dec"]]), "out");
    this.setOutput(true, "Number");
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['dht_view'] = function(block) {
  var dropdown_out = block.getFieldValue('out');
  // TODO: Assemble Lua into code variable.
  var code = dropdown_out;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

/*
 * sensor untrasonido hcsr04
 */
Blockly.Blocks['hcsr04'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Start ");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Ultrasonido");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 50, 30, "*"));
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['hcsr04'] = function(block) {
  // TODO: Assemble Lua into code variable.
  var code = `dofile("hcsr04.lc")
`;
  return code;
};

/*
 * assignment hcsr04
 */
// Blockly.Blocks['hcsr04_declare'] = {
//   init: function() {
//     this.appendDummyInput()
//         .setAlign(Blockly.ALIGN_CENTRE)
//         .appendField("Asignar");
//     this.appendDummyInput()
//         .setAlign(Blockly.ALIGN_CENTRE)
//         .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 50, 30, "*"))
//         .appendField(new Blockly.FieldVariable("item"), "NAME");
//     this.appendValueInput("trig")
//         .setCheck("Number")
//         .setAlign(Blockly.ALIGN_RIGHT)
//         .appendField("Disparo pin");
//     this.appendValueInput("echo")
//         .setCheck("Number")
//         .setAlign(Blockly.ALIGN_RIGHT)
//         .appendField("Echo pin");
//     this.appendValueInput("max_distance")
//         .setCheck("Number")
//         .setAlign(Blockly.ALIGN_RIGHT)
//         .appendField("Distancia max");
//     this.appendValueInput("avg_readings")
//         .setCheck("Number")
//         .setAlign(Blockly.ALIGN_RIGHT)
//         .appendField("Muestras");
//     this.setInputsInline(false);
//     this.setPreviousStatement(true, null);
//     this.setNextStatement(true, null);
//     this.setColour(300);
//     this.setTooltip('');
//     this.setHelpUrl('http://www.example.com/');
//   }
// };

// Blockly.Lua['hcsr04_declare'] = function(block) {
//   var variable_name = Blockly.Lua.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
//   var value_trig = Blockly.Lua.valueToCode(block, 'trig', Blockly.Lua.ORDER_ATOMIC);
//   var value_echo = Blockly.Lua.valueToCode(block, 'echo', Blockly.Lua.ORDER_ATOMIC);
//   var value_max_distance = Blockly.JavaScript.valueToCode(block, 'max_distance', Blockly.JavaScript.ORDER_ATOMIC);
//   var value_avg_readings = Blockly.JavaScript.valueToCode(block, 'avg_readings', Blockly.JavaScript.ORDER_ATOMIC);
//   // TODO: Assemble Lua into code variable.
// 	// var code = variable_name+' = HCSR04('+value_trig+', '+value_echo+', 25, 3)\n';
//   var code = `${variable_name} = HCSR04(${value_trig}, ${value_echo}, ${value_max_distance}, ${value_avg_readings})
// `
//   return code;
// };

Blockly.Blocks['hcsr04_declare'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Asignar");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 50, 30, "*"))
        .appendField(new Blockly.FieldVariable("item"), "NAME");
    this.appendValueInput("trig")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Disparo pin");
    this.appendValueInput("echo")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Echo pin");
    this.appendValueInput("max_distance")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Distancia max");
    this.appendValueInput("avg_readings")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Muestras");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

// Blockly.JavaScript['hcsr04_declare'] = function(block) {
//   var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
//   var value_trig = Blockly.JavaScript.valueToCode(block, 'trig', Blockly.JavaScript.ORDER_ATOMIC);
//   var value_echo = Blockly.JavaScript.valueToCode(block, 'echo', Blockly.JavaScript.ORDER_ATOMIC);
//   var value_max_distance = Blockly.JavaScript.valueToCode(block, 'max_distance', Blockly.JavaScript.ORDER_ATOMIC);
//   var value_avg_readings = Blockly.JavaScript.valueToCode(block, 'avg_readings', Blockly.JavaScript.ORDER_ATOMIC);
//   // TODO: Assemble JavaScript into code variable.
//   var code = `${variable_name} = HCSR04(${value_trig}, ${value_echo}, ${value_max_distance}, ${value_avg_readings})
// `
//   return code;
// };
Blockly.Lua['hcsr04_declare'] = function(block) {
  var variable_name = Blockly.Lua.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
  var value_trig = Blockly.Lua.valueToCode(block, 'trig', Blockly.Lua.ORDER_ATOMIC);
  var value_echo = Blockly.Lua.valueToCode(block, 'echo', Blockly.Lua.ORDER_ATOMIC);
  var value_max_distance = Blockly.Lua.valueToCode(block, 'max_distance', Blockly.Lua.ORDER_ATOMIC);
  var value_avg_readings = Blockly.Lua.valueToCode(block, 'avg_readings', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = `${variable_name} = HCSR04(${value_trig}, ${value_echo}, ${value_max_distance}, ${value_avg_readings})
`
  return code;
};

Blockly.Blocks['hcsr04_read'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Leer distancia");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, "*"))
        .appendField(new Blockly.FieldVariable("item"), "sensor");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['hcsr04_read'] = function(block) {
  var variable_sensor = Blockly.Lua.variableDB_.getName(block.getFieldValue('sensor'), Blockly.Variables.NAME_TYPE);
  // TODO: Assemble Lua into code variable.
  // var code = '...\n';
  var code = `${variable_sensor}.measure()
`;
  return code;
};

Blockly.Blocks['hcsr04_distance'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("De")
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, "*"))
        .appendField(new Blockly.FieldVariable("item"), "sensor")
        .appendField("obtener")
    this.appendDummyInput()
        .appendField("distancia en")
        .appendField(new Blockly.FieldDropdown([["centímetros", "distance * 100"], ["metros", "distance"]]), "escala");
    this.setOutput(true, "Number");
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['hcsr04_distance'] = function(block) {
  var variable_sensor = Blockly.Lua.variableDB_.getName(block.getFieldValue('sensor'), Blockly.Variables.NAME_TYPE);
  var dropdown_escala = block.getFieldValue('escala');
  // TODO: Assemble Lua into code variable.
  var code = `${variable_sensor}.${dropdown_escala}`;
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Lua.ORDER_NONE];
};

/*
 * hcsr04_read
 */
 // Blockly.Blocks['hcsr04_read'] = {
 //  init: function() {
 //    this.appendDummyInput()
 //        .setAlign(Blockly.ALIGN_CENTRE)
 //        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/ultra.gif", 30, 15, "*"))
 //        .appendField("Leer distancia en")
 //        .appendField(new Blockly.FieldVariable("item"), "NAME");
 //    this.setInputsInline(false);
 //    this.setOutput(true, null);
 //    this.setColour(300);
 //    this.setTooltip('');
 //    this.setHelpUrl('http://www.example.com/');
 //  }
// };

// Blockly.Lua['hcsr04_read'] = function(block) {
 //  var variable_name = Blockly.Lua.variableDB_.getName(block.getFieldValue('NAME'), Blockly.Variables.NAME_TYPE);
 //  // TODO: Assemble Lua into code variable.
 //  var code = variable_name+'.measure()';
 //  // TODO: Change ORDER_NONE to the correct strength.
 //  return [code, Blockly.Lua.ORDER_NONE];
// };

/*
 * DS18B20 INIT
 * */
Blockly.Blocks['ds18b20_lib'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/termometro.png", 25, 25, "*"))
        .appendField("Iniciar DS18B20");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("y guardar temperatura");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("En: ")
        .appendField(new Blockly.FieldVariable("item"), "temp");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['ds18b20_lib'] = function(block) {
  var variable_temp = Blockly.Lua.variableDB_.getName(block.getFieldValue('temp'), Blockly.Variables.NAME_TYPE);
  // TODO: Assemble Lua into code variable.
  var code = `
file.remove("ds18b20_save.lc")
_ds18b20 = require("ds18b20")
function readout(temps_)
  i_ = 0
  for i, s in ipairs(ds18b20.sens) do
    i_ = i
  end
  if i_ == 1 then
    for _addr, _temp in pairs(temps_) do
      ${variable_temp} = _temp
    end
  else
    ${variable_temp} = -255
  end
end
`;
  return code;
};

/*
 * DS18B20 READ
 * */

Blockly.Blocks['ds18b20_read'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/termometro.png", 25, 25, "*"))
        .appendField("Leer DS18B20");
    this.appendValueInput("Pin")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("En el Pin");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("en la escala")
        .appendField(new Blockly.FieldDropdown([["Celsius", "ds18b20.C"], ["Kelvin", "ds18b20.K"], ["Fahrenheit", "ds18b20.F"]]), "scale");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['ds18b20_read'] = function(block) {
  var value_pin = Blockly.Lua.valueToCode(block, 'Pin', Blockly.Lua.ORDER_ATOMIC);
  var dropdown_scale = block.getFieldValue('scale');
  // TODO: Assemble Lua into code variable.
  var code = `_ds18b20:read_temp(readout, ${value_pin}, ${dropdown_scale})
_ds18b20:read_temp(readout, ${value_pin})
`;
  return code;
};
