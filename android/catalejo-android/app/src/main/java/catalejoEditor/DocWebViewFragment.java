package catalejoEditor;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class DocWebViewFragment extends Fragment {
  protected @Nullable
  WebView docsWebView = null;

  @SuppressLint("SetJavaScriptEnabled")
  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    docsWebView = new WebView(inflater.getContext());
    docsWebView.setWebChromeClient(new WebChromeClient());
    // Enable Javascript in the WebView
    WebSettings webSettings = docsWebView.getSettings();
    webSettings.setJavaScriptEnabled(true);
    // Enable interface for methods java in javascript
    // mWebView.addJavascriptInterface(new WebAppInterface((DocumentationActivity)getActivity()), "Doc");
    // load some page in the WebView
    docsWebView.loadUrl("file:///android_asset/blockly/docs/index.html");
    return docsWebView;
  }
}
