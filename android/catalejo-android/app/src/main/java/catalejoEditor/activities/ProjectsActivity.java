package catalejoEditor.activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.blocklywebview.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import catalejoEditor.Data;

public class ProjectsActivity extends AppCompatActivity {

  /** Documents **/
  // Unique request code.
  private static final int READ_REQUEST_CODE = 42;
  private static final int SAVEAS_REQUEST_CODE = 43;
  private static final int NEW_REQUEST_CODE = 44;
  private static final int SHARE_REQUEST_CODE = 45;

  /** datas **/
  private Data data = new Data();
  private Uri projectUri;

  /** Manejo de datos entre actividades **/
  // private Bundle extra; // Comunicación de datos en distintas Actividades

  /**TEXT**/
  TextView filenameInput;    //Entrada de nombre de archivo

  /** BUTTONS **/
  // ImageButton returnButton; //Botón que permite retornar a la actividad principal
  Button returnButton; //Botón que permite retornar a la actividad principal
  ImageButton newButton; // Guardar nuevos proyectos
  // Button saveButton; //Guarda el archivo en el storage
  ImageButton saveAsButton; // Abre el direcorio y selecciona el archivo a ser cargado
  ImageButton openButton; // Carga el archivo
  ImageButton shareButton; // Compartir archivo

  /** LinearLayout **/
  LinearLayout newLinearLayout; // Nuevo proyecto
  LinearLayout openLinearLayout; // Abrir proyecto
  LinearLayout saveLinearLayout; // Guardar como
  LinearLayout shareLinearLayout; // compartir

  private static final String LOG_TAG = ProjectsActivity.class.getSimpleName(); // Registro para la actividad instanciada
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Log.d(LOG_TAG,"catalejo-msg: construyendo actividad Projects");
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_projects);

    Intent intent = getIntent();
    projectUri = intent.getData();
    // extra = intent.getExtras();
    // data.setData(extra);  // Toma los valores traidos de la Actividad Main y los asigna a la variable data
    data.setDataFromActivity(intent.getExtras());  // Toma los valores traidos de la Actividad Main y los asigna a la variable data
    Log.d(LOG_TAG, "catalejo-msg: Obteniendo código traído de main activity?: ");

    /** TextView **/
    filenameInput = findViewById(R.id.filenameProjectDisplay);
    if(data.getFilename() != "") {
      filenameInput.setText(data.getFilename());
    }else{
      filenameInput.setText("Ruta donde se guardará el proyecto...");
    }

    /** LISTENER BUTTONS **/
    addListenerOnReturnButton();
    addListenerOnNewButton();
    // addListenerOnSaveButton();
    addListenerOnSaveAsButton();
    addListenerOnOpenButton();
    addListenerOnShareButton();

    /** LAYOUT DEFINITIONS **/
    newLinearLayout = findViewById(R.id.newLinearLayout);
    openLinearLayout = findViewById(R.id.openLinearLayout);
    saveLinearLayout = findViewById(R.id.saveLinearLayout);
    shareLinearLayout = findViewById(R.id.shareLinearLayout);

    // visibilidad de botones segun atividad desde donde ha sido llamado
    configButtonsVisibility(data);

  }

  // CONFIGURE BUTTONS VISIBILITY
  public void configButtonsVisibility(Data data) {
    filenameInput.setVisibility(View.GONE); // TODO botón invisible, definir pertinencia
    if (data.getMessageBetweenActivities() == Data.MessageBetweenActivities.FROM_HOME) {
      // saveAsButton.setVisibility(View.INVISIBLE); // Ocultar el boton guardar como
      saveLinearLayout.setVisibility(View.GONE);
      if (projectUri == null){
        shareLinearLayout.setVisibility(View.GONE);
      }
    } else if (data.getMessageBetweenActivities() == Data.MessageBetweenActivities.FROM_SAVE_BLOCKLY) {
      // newButton.setVisibility(View.INVISIBLE); // Ocultar el boton de nuevo proyecto
      // openButton.setVisibility(View.INVISIBLE); // Ocultar el boton de abrir proyecto
      newLinearLayout.setVisibility((View.GONE));
      openLinearLayout.setVisibility(View.GONE);
      shareLinearLayout.setVisibility(View.GONE);
    } else if (data.getMessageBetweenActivities() == Data.MessageBetweenActivities.FROM_PROJECT_BLOCKLY) {
      // newButton.setVisibility(View.INVISIBLE); // Ocultar el botón de crear nuevo proyecto
      newLinearLayout.setVisibility(View.GONE);
      if (projectUri == null){
        shareLinearLayout.setVisibility(View.GONE);
      }
    }
  }

  /** LISTENER NEW BUTTON  **/
  public void addListenerOnNewButton(){
    newButton = findViewById(R.id.newProjectButton);
    newButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        newFileFromDocuments();
      }
    });
  }
  public void newFileFromDocuments(){
    createFile("*/*", "myProject.txt");
  }

  /** LISTENER SAVE BUTTON  **/
  /** public void addListenerOnSaveButton(){
    saveButton = findViewById(R.id.saveButton);
    saveButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        saveFileFromDocuments();
      }
    });
  }
  public void saveFileFromDocuments(){
    if (data.getFilename() != ""){
      //saveJSONFile(data.getFilename(), JSON2StringData(data));
      saveJSONFileFromURI(data.getUri(), JSON2StringData(data));
      Toast.makeText(getApplicationContext(), "Guardando actualización en archivo", Toast.LENGTH_SHORT).show();
    }else{
      newFileFromDocuments();
    }
  } **/

  /** LISTENER SAVEAS BUTTON  **/
  public void addListenerOnSaveAsButton(){
    saveAsButton = findViewById(R.id.saveAsProjectButton);
    saveAsButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        saveAsFileFromDocuments();
      }
    });
  }
  public void saveAsFileFromDocuments(){
    saveAsFile("*/*", "myProject.txt");
  }

  /** LISTENER OPEN BUTTON  **/
  public void addListenerOnOpenButton(){
    openButton = findViewById(R.id.openProjectButton);
    openButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        openFileFromDocuments();
      }
    });
  }
  public void openFileFromDocuments(){
    performFileSearch();
  }

  /** LISTENER SHARE BUTTON  **/
  public void addListenerOnShareButton(){
    shareButton = findViewById(R.id.shareProjectButton);
    shareButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        shareFile();
      }
    });
  }

  public void shareFile(){
    Intent intent = new Intent(Intent.ACTION_SEND);
    intent.putExtra(Intent.EXTRA_STREAM, this.projectUri);
    intent.setType("text/plain");
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    // startActivity(Intent.createChooser(intent, "Compartiendo proyecto")); // FUNCIONAL
    this.startActivityForResult(Intent.createChooser(intent, "Compartir"),
        SHARE_REQUEST_CODE);
  }

  /** LISTENER RETURN BUTTON  **/
  public void addListenerOnReturnButton(){
    returnButton = findViewById(R.id.returnButton);
    returnButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.d(LOG_TAG, "catalejo-msg: return to principal");
        Uri uri = Uri.EMPTY;
        returnMain(Data.MessageBetweenActivities.PASS, uri); // Retorna a la actividad sin cambios
      }
    });
  }

  public void returnMain(Data.MessageBetweenActivities resultMessage, Uri uri){

    Intent returnToMain = new Intent(); // Asigna memoria para retornar datos a la Actividad Main

    Log.d(LOG_TAG, "catalejo-msg: uri " + data.getUri());
    Log.d(LOG_TAG, "catalejo-msg: fileName " + data.getFilename());

    // verificar si se ha cancelado las acciones de guardar o abrir
    if (resultMessage == Data.MessageBetweenActivities.PASS){
      setResult(RESULT_CANCELED, returnToMain); // Se canceló guardar o abrir
      finish(); // Se detienen la actividad ProjectsActivity
      // se solicita retornar a la actividad de llamada
    } else if (resultMessage == Data.MessageBetweenActivities.RETURN) {
      returnToMain.setData(uri);
      returnToMain.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
      // se verifica si se debe lanzar la actividad blockly
      if (data.getMessageBetweenActivities() == Data.MessageBetweenActivities.FROM_HOME){
        // En este caso, al guardar, se solicita ir a la actividad de blockly
        data.setMessageBetweenActivities(Data.MessageBetweenActivities.GO_TO_BLOCKLY);
      } else if (data.getMessageBetweenActivities() == Data.MessageBetweenActivities.FROM_PROJECT_BLOCKLY){
        // Al guardar, deberá reiniciar el blockly
        data.setMessageBetweenActivities(Data.MessageBetweenActivities.RESTART_BLOCKLY);
      } else {
        // se retorna a la actividad que hizo la llamada
        data.setMessageBetweenActivities(Data.MessageBetweenActivities.RETURN);
      }
      returnToMain.putExtras(data.getExtraBundleFromData()); // Se inyecta el bundle en el Intent
      setResult(RESULT_OK, returnToMain); // Se entrega el resultado OK con el Intent
      finish(); // Se detienen la actividad ProjectsActivity
      Log.d(LOG_TAG, "catalejo-msg: finalizando la actividad projects");
    }
  }

  // Here are some examples of how you might call this method.
  // The first parameter is the MIME type, and the second parameter is the name
  // of the file you are creating:
  //
  // createFile("text/plain", "foobar.txt");
  // createFile("image/png", "mypicture.png");
  private void createFile(String mimeType, String fileName) {
    Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);

    // Filter to only show results that can be "opened", such as
    // a file (as opposed to a list of contacts or timezones).
    intent.addCategory(Intent.CATEGORY_OPENABLE);

    // Create a file with the requested MIME type.
    intent.setType(mimeType);
    intent.putExtra(Intent.EXTRA_TITLE, fileName);
    startActivityForResult(intent, NEW_REQUEST_CODE);
  }

  private void saveAsFile(String mimeType, String fileName) {
    Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);

    // Filter to only show results that can be "opened", such as
    // a file (as opposed to a list of contacts or timezones).
    intent.addCategory(Intent.CATEGORY_OPENABLE);

    // Create a file with the requested MIME type.
    intent.setType(mimeType);
    intent.putExtra(Intent.EXTRA_TITLE, fileName);
    startActivityForResult(intent, SAVEAS_REQUEST_CODE);
  }

  /**
   * Fires an intent to spin up the "file chooser" UI and select an image.
   */
  public void performFileSearch() {
    // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
    // browser.
    // Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
    // Filter to only show results that can be "opened", such as a
    // file (as opposed to a list of contacts or timezones)
    // intent.addCategory(Intent.CATEGORY_OPENABLE);
    // Filter to show only images, using the image MIME data type.
    // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
    // To search for all documents available via installed storage providers,
    // it would be "*/*", "text/plain".
    intent.setType("*/*");
    startActivityForResult(intent, READ_REQUEST_CODE);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
  
    // DOCUMENTIACIÓN RESUMIDA DE ACTIVIDADES
    // https://developer.android.com/reference/android/app/Activity#summary

    // The ACTION_OPEN_DOCUMENT intent was sent with the request code
    // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
    // response to some other intent, and the code below shouldn't run at all.

    if (requestCode == READ_REQUEST_CODE && resultCode == RESULT_OK) {
      // The document selected by the user won't be returned in the intent.
      // Instead, a URI to that document will be contained in the return intent
      // provided to this method as a parameter.
      // Pull that URI using resultData.getData().
      if (resultData != null) {
        Uri uri = resultData.getData();
        // data.setUri(resultData.getData());

        //String realPath = getRealPathFromURI(getApplicationContext(), uri);
        //if (updateDataFromJSON(stringData2JSONData(getStringFromFile(realPath))) == true) { // Actualiza el objeto data de la clase Data
        //  data.setFilename(realPath);
        //  filenameInput.setText(data.getFilename());
        //  Toast.makeText(getApplicationContext(), "Archivo abierto", Toast.LENGTH_SHORT).show();
        //}

        String stringFile = "";
        try {
          stringFile = readFile(uri);
        } catch (IOException e) {
          e.printStackTrace();
        }
        if (updateDataFromJSON(stringData2JSONData(stringFile)) == true) { // Actualiza el objeto data de la clase Data
          //data.setFilename(realPath);
          filenameInput.setText(data.getFilename());
          data.setUri(uri);
          Toast.makeText(getApplicationContext(), "Archivo abierto", Toast.LENGTH_SHORT).show();
          returnMain(Data.MessageBetweenActivities.RETURN, uri);
        }
      }
    }else if (requestCode == SAVEAS_REQUEST_CODE && resultCode == RESULT_OK){
      if (resultData != null){
        Uri uri = resultData.getData();

        //String realPath = getRealPathFromURI(getApplicationContext(), uri);
        //saveJSONFile(uri.getPath(), JSON2StringData(data)); // Se guarda el proyecto con el nombre determinado
        //data.setFilename(realPath); // Al crear el nuevo archivo se guarda también el Filename

        saveJSONFileFromURI(uri, JSON2StringData(data));
        filenameInput.setText(data.getFilename());
        data.setUri(uri);
        Toast.makeText(getApplicationContext(), "Proyecto guardado", Toast.LENGTH_SHORT).show();
        returnMain(Data.MessageBetweenActivities.RETURN, uri);
      } else {
        Log.d(LOG_TAG, "catalejo-msg: se aborta guardar mensaje");
      }
    }else if (requestCode == NEW_REQUEST_CODE && resultCode == RESULT_OK){
      if (resultData != null){
        Uri uri = resultData.getData();

        //String realPath = getRealPathFromURI(getApplicationContext(), uri);
        //data.setData("", "<xml xmlns=\"http://www.w3.org/1999/xhtml\"></xml>",realPath);
        //saveJSONFile(realPath, JSON2StringData(data)); // Se guarda el proyecto con el nombre determinado
        //data.setFilename(realPath); // Al crear el nuevo archivo se guarda también el Filename
        // Ajustando datas para nuevo proyectos 
        data.setData("", "<xml xmlns=\"http://www.w3.org/1999/xhtml\"></xml>", uri);
        saveJSONFileFromURI(uri, JSON2StringData(data));
        data.setFilename(uri.getPath()); // Al crear el nuevo archivo se guarda también el Filename
        data.setUri(uri);
        filenameInput.setText(data.getFilename());
        Toast.makeText(getApplicationContext(), "Nuevo proyecto iniciado", Toast.LENGTH_SHORT).show();
        returnMain(Data.MessageBetweenActivities.RETURN, uri);
      }
    } else if (requestCode == SHARE_REQUEST_CODE && resultCode == RESULT_OK){
      Log.d(LOG_TAG, "catalejo-msg: Compartiendo archivo");
      Toast.makeText(getApplicationContext(), "Compartiendo proyecto",
          Toast.LENGTH_SHORT).show();
    } else if (resultCode == RESULT_CANCELED){
      Uri uri = Uri.EMPTY;
      returnMain(Data.MessageBetweenActivities.PASS, uri);
    }
  }

  /** Obtener path real desde Uri **/
  private String getRealPathFromURI(Context context, Uri contentUri) {
    Cursor cursor = null;
    try {
      String[] proj = { MediaStore.Images.Media.DATA };
      cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
      int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
      cursor.moveToFirst();
      return cursor.getString(column_index);
    } catch (Exception e) {
      Log.e(LOG_TAG, "getRealPathFromURI Exception : " + e.toString());
      return "";
    } finally {
      if (cursor != null) {
        cursor.close();
      }
    }
  }

  /** Obtener string desde archivo **/
  private String getStringFromFile(String pathFile) {
    String dataString = "";
    try {
      File file = new File(pathFile);
      try {
        FileInputStream fileInputStream = new FileInputStream(file);
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        dataString = bufferedReader.readLine();
        fileInputStream.close();
      }catch (IOException e){
        Log.d(LOG_TAG, "No se puede leer archivo");
      }
    }catch (Throwable e){
      Log.d(LOG_TAG, "No se puede abrir archivo");
    }
    return dataString;
  }

  /** Transforma obj JSON a Data Obj **/
  private boolean updateDataFromJSON(JSONObject obj){
    try {
      data.setCode(obj.get("CODE").toString());
      data.setBlocklyCode(obj.get("BLOCKLY").toString());
    }catch (Throwable e){
      Log.d(LOG_TAG, "catalejo-msg: "+e.toString());
    }
    return true;
  }

  /** Crear JSON a partir de data **/
  private JSONObject stringData2JSONData(String stringData){
    JSONObject obj = null;
    try{
      obj = new JSONObject(stringData);
    }catch (Throwable e){
      Log.d(LOG_TAG, "No se construye JSON a partir String");
    }
    return obj;
  }

  /** Crear el JSONString desde los data **/
  private String JSON2StringData(Data data){
    JSONObject obj = new JSONObject();
    try {
      obj.put("CODE", data.getCode());
      try{
        obj.put("BLOCKLY", data.getBlocklyCode());
      }catch (JSONException e){
        Log.d(LOG_TAG, "No se agrega BLOCKLY a obj JSON");
      }
    }catch (JSONException e){
      Log.d(LOG_TAG, "No se agrega CODE a obj JSON");
    }
    return obj.toString();
  }

  /** Guardando JSON en File**/
  private void saveJSONFile(String path, String dataToFile){
    Log.d(LOG_TAG, dataToFile);

    try {
      File file = new File(path);
      if (!file.exists()) {
        file.createNewFile();
      }
      FileWriter writer = new FileWriter(file);
      writer.append(dataToFile);
      writer.flush();
      writer.close();
    }catch (IOException e){
      Log.d(LOG_TAG, "catalejo-msg:"+e.getMessage());
    }
  }

  private void saveJSONFileFromURI(Uri uri, String dataToFile) {
    try {
      ParcelFileDescriptor pfd = getContentResolver().
        openFileDescriptor(uri, "w");
      FileOutputStream fileOutputStream =
        new FileOutputStream(pfd.getFileDescriptor());
      fileOutputStream.write((dataToFile).getBytes());
      // Let the document provider know you're done by closing the stream.
      fileOutputStream.close();
      pfd.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private String readFile(Uri uri) throws IOException {
    ParcelFileDescriptor parcelFileDescriptor =
      getContentResolver().openFileDescriptor(uri, "r");
    FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
    FileInputStream fileInputStream =
      new FileInputStream(fileDescriptor);
    int i=0;
    String readFileContent = "";
    while((i=fileInputStream.read())!=-1){
      readFileContent = readFileContent + ((char)i);
    }
    Log.d(LOG_TAG, "catalejo-msg: Obteniendo código traído de WebHtml: "+ readFileContent);
    parcelFileDescriptor.close();
    return readFileContent;
  }

}
