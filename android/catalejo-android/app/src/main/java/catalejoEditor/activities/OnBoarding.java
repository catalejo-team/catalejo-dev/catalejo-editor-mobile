package catalejoEditor.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.blocklywebview.R;

import android.view.Gravity;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.synnapps.carouselview.ViewListener;
import android.widget.ImageView;

public class OnBoarding extends AppCompatActivity {

  /** BUTTONS **/
  Button skipButton; // Botón para salir de esta actividad e ir a la actividad home

  //public static Context mContext;
  private static final String LOG_TAG = OnBoarding.class.getSimpleName();

  CarouselView carouselView;
  private int[] gallery = {
    R.drawable.catalejoplus_press, // Press catalejo
    R.drawable.programacion_grafica, // Programacion
    R.drawable.kit, //kit
    R.drawable.detector_sed_planta, // Detector de sed
  };
  String[] titleDescriptionText = {
    "Catalejo Editor", // Press catalejo
    "Programación gráfica", // Programacion
    "¿Qué tipo de proyectos podrías realizar?", //kit
    "¿Tu planta tiene sed?",  // Detector de sed
  };
  String[] descriptionText = {
    "Para más información escríbenos a hola@catalejoplus.com o vísitanos en www.catalejoplus.com", // Press Catalejo
    "Programación intuitiva por bloques representativos de hardware.", // Programación
    "Crea carros controlados remotamente, brazos robóticos, estaciones metereológicas y más.", //kit
    "Construye un detector de sed para tu planta, así podrás regarla cuando sea necesario." // Detector de sed
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_on_boarding);

    carouselView = findViewById(R.id.carouselView);
    carouselView.setPageCount(gallery.length);
    carouselView.setSlideInterval(6000);
    // carouselView.setImageListener(imageListener);
    carouselView.setViewListener(viewListener);

    /** LISTENER BUTTONS **/
    addListenerSkipButton();
  }

  /** LISTENER SKIP_BUTTON **/
  public void addListenerSkipButton() {
    skipButton = findViewById(R.id.skipButton);
    skipButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startHomeActivity();
      }
    });
  }
  public void startHomeActivity() {
    Intent intent = new Intent(this, HomeActivity.class);
    startActivity(intent);
    finish();
  }

  ViewListener viewListener = new ViewListener() {
    @Override
    public View setViewForPosition(int position) {
      View customView = getLayoutInflater().inflate(R.layout.onboarding_view_custom, null);

      TextView onBoardingLabelTextView = customView.findViewById(R.id.onBoardingLabelTextView);
      TextView onBoardingLabelTitleDescription = customView.findViewById(R.id.onBoardingLabelTitleDescription);
      ImageView onBoardingImageView = customView.findViewById(R.id.onBoardingImageView);

      onBoardingImageView.setImageResource(gallery[position]);
      onBoardingLabelTextView.setText(descriptionText[position]);
      onBoardingLabelTitleDescription.setText(titleDescriptionText[position]);
      carouselView.setIndicatorGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP);

      return customView;
    }
  };
}
