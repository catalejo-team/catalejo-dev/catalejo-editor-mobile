package catalejoEditor.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.net.Uri;
import android.content.Intent;

import com.example.blocklywebview.R;

import catalejoEditor.Data;

public class HomeActivity extends AppCompatActivity {

  /** BUTTONS **/
  Button blocklyButton; // Botón para salir de esta actividad e ir a la actividad home
  Button projectsButton; // Botón para salir de esta actividad e ir a la actividad projects
  Button docsButton; // Boton para inicial la actvidad de documentación
  Button tourButton; // Botón para ir al tour de inicio

  //public static Context mContext;
  private static final String LOG_TAG = HomeActivity.class.getSimpleName();
  private static final int REQUEST_MAIN_ACTIVITY = 1;
  private static final int REQUEST_PROJECTS_ACTIVITY = 2;
  private static final int REQUEST_DOC_ACTIVITY = 3;

  private Data data;
  private Uri projectUri;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);

    data = new Data();
    Log.d(LOG_TAG, "catalejo-msg: Created Home Activity");

    /** LISTENER BUTTONS **/
    addListenerBlocklyButton();
    addListenerProjectsButton();
    addListenerDocsButton();
    addListenerTourButton();
  }

  /** LISTENER BLOCKLY_BUTTON **/
  public void addListenerBlocklyButton() {
    blocklyButton = findViewById(R.id.blocklyButton);
    blocklyButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startMainActivity();
      }
    });
  }

  public void startMainActivity() {
    // https://commonsware.com/blog/2020/08/08/uri-access-lifetime-still-shorter-than-you-might-think.html
    Log.d(LOG_TAG, "catalejo-msg: creacion actividad main");
    Intent intent = new Intent(this.getApplicationContext(), MainActivity.class);
    intent.setData(this.projectUri);
    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
    intent.putExtras(this.data.getExtraBundleFromData());
    this.startActivityForResult(intent, REQUEST_MAIN_ACTIVITY);
  }

  /** LISTENER PROJECTS_BUTTON **/
  public void addListenerProjectsButton() {
    projectsButton = findViewById(R.id.projectsButton);
    projectsButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startProjectsActivity();
      }
    });
  }

  public void startProjectsActivity() {
    this.data.setMessageBetweenActivities(Data.MessageBetweenActivities.FROM_HOME);
    Log.d(LOG_TAG, "catalejo-msg: creacion actividad projects");
    Intent intent = new Intent(this.getApplicationContext(), ProjectsActivity.class);
    intent.setData(this.projectUri);
    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
    intent.putExtras(this.data.getExtraBundleFromData());
    this.startActivityForResult(intent, REQUEST_PROJECTS_ACTIVITY);
  }

  /** LISTENER DOCS_BUTTON **/
  public void addListenerDocsButton() {
    docsButton = findViewById(R.id.docsButton);
    docsButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startDocumentationActivity();
      }
    });
  }

  public void startDocumentationActivity() {
    Log.d(LOG_TAG, "catalejo-msg: creacion actividad docs");
    Intent intent = new Intent(this.getApplicationContext(), DocumentationActivity.class);
    intent.putExtras(this.data.getExtraBundleFromData());
    this.startActivityForResult(intent, REQUEST_DOC_ACTIVITY);
  }

  /** LISTENER TOUR_BUTTON **/
  public void addListenerTourButton() {
    tourButton = findViewById(R.id.tourButton);
    tourButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        // Log.d(LOG_TAG, "catalejo-msg: " + data.getUrlDocs());
        data.setUrlDocs(" file:///android_asset/blockly/docs/intro/blink/index.html");
        startDocumentationActivity();
      }
    });
  }

  //******************************************************************************
  // ** START ** RETORNOS DE ACTIVIDADES LLAMADAS ** START **
  //******************************************************************************
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
    Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad");
    if (resultCode == RESULT_OK){
      Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad RESULT_OK!");
      if (requestCode == REQUEST_MAIN_ACTIVITY){
        Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad main");
        data.setDataFromActivity(resultData.getExtras());
        projectUri = resultData.getData();
        if (data.getMessageBetweenActivities() == Data.MessageBetweenActivities.RESTART_BLOCKLY) {
          data.setMessageBetweenActivities(Data.MessageBetweenActivities.PASS);
          startMainActivity();
        }
      } else if (requestCode == REQUEST_PROJECTS_ACTIVITY){
        Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad projects");
        data.setDataFromActivity(resultData.getExtras());
        projectUri = resultData.getData();
        if (data.getMessageBetweenActivities() == Data.MessageBetweenActivities.GO_TO_BLOCKLY) {
          startMainActivity();
        }
      } else if (requestCode == REQUEST_DOC_ACTIVITY){
        Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad docs");
        data.setDataFromActivity(resultData.getExtras());
      }
    } else if (resultCode == RESULT_CANCELED){
      Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad RESULT_CANCELED!");
    }
  }

  /* @Override
  public void onBackPressed() {
    // pass
  } **/

}
